﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;

public class UiManager : MonoBehaviour
{
    public GameObject loginForm;
    public GameObject registerForm;
    public GameObject forgotForm;
    public GameObject mainForm;
    public GameObject showNoteForm;
    public GameObject notePanel;
    public GameObject notePrefab;
    public GameObject sortBtn;
    public TMP_InputField searchField;
    public static UiManager instance;
    private int sortingType = 0;

    void Awake()
    {
        instance = this;
        //By default, the first opens the Login window
        ShowWindow("login");
    }

    /// <summary>
    /// Turns on the display of the selected window, turns off the others
    /// </summary>
    /// <param name="_window"></param>
    public void ShowWindow(string _window)
    {
        loginForm.SetActive(false);
        registerForm.SetActive(false);
        forgotForm.SetActive(false);
        mainForm.SetActive(false);
        showNoteForm.SetActive(false);

        switch (_window)
        {
            case "login":
                loginForm.SetActive(true);
                break;
            case "register":
                registerForm.SetActive(true);
                break;
            case "forgot":
                forgotForm.SetActive(true);
                break;
            case "main":
                mainForm.SetActive(true);
                //Each time notes are updated, the Database is also updated
                FirebaseManager.instance.SaveDataBase();
                //The main window displays all notes, including search and sort options
                ShowAllNotes();
                break;
            case "shownote":
                showNoteForm.SetActive(true);
                break;
        }
    }

    /// <summary>
    /// Display notes with selected parameters (search, sort, all notes)
    /// </summary>
    public void ShowAllNotes()
    {
        //Clear old notes
        if (notePanel.transform.childCount > 0)
        {
            foreach (Transform child in notePanel.transform) Destroy(child.gameObject);
        }

        //Searching notes
        List<Note> showNotes = new List<Note>();
        if (searchField.text != "")
        {
            string searchString = searchField.text;
            var selectedNotes = FirebaseManager.userData.notes.Where(t => t.name.Contains(searchString));
            foreach (Note note in selectedNotes)
            {
                showNotes.Add(note);
            }
        }
        else
        {
            foreach(Note note in FirebaseManager.userData.notes)
            {
                showNotes.Add(note);
            }
        }

        //Sorting notes
        List<Note> sortNotes = new List<Note>();
        if (sortingType != 0)
        {
            if (sortingType == 1)
            {
                //a-z
                sortNotes = showNotes.OrderBy(u => u.name).ToList();
            }
            else
            {
                //z-a
                sortNotes = showNotes.OrderByDescending(u => u.name).ToList();
            }
        }
        else 
        {
            foreach (Note note in showNotes)
            {
                sortNotes.Add(note);
            }
        }

        //Show all notes
        GameObject noteObject;
        foreach (Note note in sortNotes)
        {
            noteObject = Instantiate(notePrefab, new Vector3(0, 0, 0), Quaternion.identity, gameObject.transform);
            noteObject.gameObject.GetComponentInChildren<Text>().text = note.name;
            noteObject.gameObject.GetComponent<Button>().onClick.AddListener(() => ClickNote(note.uid));
            noteObject.transform.SetParent(notePanel.transform, false);
        }
    }


    /// <summary>
    /// Click on Note
    /// </summary>
    /// <param name="_uid"></param>
    public void ClickNote(int _uid)
    {
        NoteManager.instance.ShowNote(_uid);
        ShowWindow("shownote");
    }

    /// <summary>
    /// Search note
    /// </summary>
    public void ClickSearch()
    {
        ShowAllNotes();
    }

    /// <summary>
    /// Click on the sort. Select sorting mode according to previous
    /// </summary>
    public void ClickSorting()
    {
        if (sortingType == 1)
        {
            sortBtn.GetComponentInChildren<Text>().text = "A-Z";
            sortingType = 2;
        }
        else
        {
            sortBtn.GetComponentInChildren<Text>().text = "Z-A";
            sortingType = 1;
        }
        ShowAllNotes();
    }


}
