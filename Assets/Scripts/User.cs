using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class User
{
    public string username;
    public List<Note> notes;

    public User(string _username, List<Note> _notes)
    {
        this.username = _username;
        this.notes = _notes;
    }
}
