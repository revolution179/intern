﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Database;
using Firebase.Storage;
using Firebase.Auth;
using TMPro;


public class FirebaseManager : MonoBehaviour
{
    //Firebase variable
    [Header("Firebase")]
    public DependencyStatus dependencyStatus;
    public FirebaseAuth auth;
    public FirebaseUser User;
    public DatabaseReference DBreference;

    //Login variables
    [Header("Login")]
    public TMP_InputField emailLoginField;
    public TMP_InputField passwordLoginField;
    public TMP_Text warningLoginText;
    public TMP_Text confirmLoginText;

    //Register variables
    [Header("Register")]
    public TMP_InputField usernameRegisterField;
    public TMP_InputField emailRegisterField;
    public TMP_InputField passwordRegisterField;
    public TMP_InputField passwordRegisterVerifyField;
    public TMP_Text warningRegisterText;

    //Forgot password variables
    [Header("Forgot")]
    public TMP_InputField emailForgotField;
    public TMP_Text warningForgotText;
    public TMP_Text confirmForgotText;

    //User data variables
    [Header("User")]
    public TMP_Text usernameText;

    public static User userData;
    public static FirebaseManager instance;

    private void Awake()
    {
        instance = this;
        //Initialize Firebase (Task)
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            dependencyStatus = task.Result;
            if (dependencyStatus == DependencyStatus.Available)
            {
                InitializeFirebase();
            }
            else
            {
                Debug.LogError("Could not resolve all Firebase dependencies: " + dependencyStatus);
            }
        });
    }

    /// <summary>
    /// Initialize firebase and firebase database
    /// </summary>
    private void InitializeFirebase()
    {
        Debug.Log("Setting up Firebase Auth");
        auth = FirebaseAuth.DefaultInstance;
        DBreference = FirebaseDatabase.DefaultInstance.RootReference;
    }

    /// <summary>
    /// Click login button
    /// </summary>
    public void LoginButton()
    {
        StartCoroutine(Login(emailLoginField.text, passwordLoginField.text));
    }

    /// <summary>
    /// Click register button
    /// </summary>
    public void RegisterButton()
    {
        StartCoroutine(Register(emailRegisterField.text, passwordRegisterField.text, usernameRegisterField.text));
    }

    /// <summary>
    /// Click forgot password button
    /// </summary>
    public void ForgotButton()
    {
        StartCoroutine(Forgot(emailForgotField.text));
    }

    /// <summary>
    /// Save Firebase Database
    /// </summary>
    public void SaveDataBase()
    {
        StartCoroutine(RecordDatabase());
    }

    /// <summary>
    /// Loading Firebase Database
    /// </summary>
    public void LoadDataBase()
    {
        StartCoroutine(ReadDatabase());
    }

    /// <summary>
    /// Start downloading images to Firebase Storage
    /// </summary>
    /// <param name="_image"></param>
    /// <param name="_uid"></param>
    public void StartUploadImage(Texture2D _image, int _uid)
    {
        StartCoroutine(UploadImage(_image, _uid));
    }

    /// <summary>
    /// Click logout button
    /// </summary>
    public void LogoutButton()
    {
        //log out of your account
        auth.SignOut();
        UiManager.instance.ShowWindow("login");
        userData = null;
        ClearTextFields();
    }

    /// <summary>
    /// Сlearing authorization fields
    /// </summary>
    public void ClearTextFields()
    {
        warningLoginText.text = "";
        confirmLoginText.text = "";
        warningRegisterText.text = "";
        warningForgotText.text = "";
        confirmForgotText.text = "";
    }

    /// <summary>
    /// Implementation of standard Firebase solution for "User Login" situation
    /// </summary>
    /// <param name="_email"></param>
    /// <param name="_password"></param>
    /// <returns></returns>
    private IEnumerator Login(string _email, string _password)
    {
        var LoginTask = auth.SignInWithEmailAndPasswordAsync(_email, _password);
        yield return new WaitUntil(predicate: () => LoginTask.IsCompleted);
        if (LoginTask.Exception != null)
        {
            Debug.LogWarning(message: $"Failed to register task with {LoginTask.Exception}");
            FirebaseException firebaseEx = LoginTask.Exception.GetBaseException() as FirebaseException;
            AuthError errorCode = (AuthError)firebaseEx.ErrorCode;
            string message = "Login Failed!";
            switch(errorCode)
            {
                case AuthError.MissingEmail:
                    message = "Missing Email";
                    break;
                case AuthError.MissingPassword:
                    message = "Missing Password";
                    break;
                case AuthError.WrongPassword:
                    message = "Wrong Password";
                    break;
                case AuthError.InvalidEmail:
                    message = "Invalid Email";
                    break;
                case AuthError.UserNotFound:
                    message = "Account does not exist";
                    break;
            }
            warningLoginText.text = message;
            confirmLoginText.text = "";
        }
        else
        {
            User = LoginTask.Result;
            Debug.LogFormat("User signed in successfully: {0} ({1})", User.DisplayName, User.Email);
            warningLoginText.text = "";
            confirmLoginText.text = "Log in";
            LoadDataBase();
            yield return new WaitForSeconds(1);
            UiManager.instance.ShowWindow("main");
            confirmLoginText.text = "";
            usernameText.text = "Hello, "+User.DisplayName;
            
            
        }
    }

    /// <summary>
    /// Implementation of standard Firebase solution for "User Registration" situation
    /// </summary>
    /// <param name="_email"></param>
    /// <param name="_password"></param>
    /// <param name="_username"></param>
    /// <returns></returns>
    private IEnumerator Register(string _email, string _password, string _username)
    {
        if (_username == "")
        {
            warningRegisterText.text = "Missing Username";
        }
        else if (passwordRegisterField.text != passwordRegisterVerifyField.text)
        {
            warningRegisterText.text = "Password Does Not Match!";
        }
        else
        {
            var RegisterTask = auth.CreateUserWithEmailAndPasswordAsync(_email, _password);
            yield return new WaitUntil(predicate: () => RegisterTask.IsCompleted);

            if (RegisterTask.Exception != null)
            {
                Debug.LogWarning(message: $"Failed to register task with {RegisterTask.Exception}");
                FirebaseException firebaseEx = RegisterTask.Exception.GetBaseException() as FirebaseException;
                AuthError errorCode = (AuthError)firebaseEx.ErrorCode;

                string message = "Register Failed";
                switch (errorCode)
                {
                    case AuthError.MissingEmail:
                        message = "Missing Email";
                        break;
                    case AuthError.MissingPassword:
                        message = "Missing Password";
                        break;
                    case AuthError.WeakPassword:
                        message = "Weak Password";
                        break;
                    case AuthError.EmailAlreadyInUse:
                        message = "Email Already In Use";
                        break;
                }
                warningRegisterText.text = message;
            }
            else
            {
                User = RegisterTask.Result;

                if (User != null)
                {
                    UserProfile profile = new UserProfile { DisplayName = _username };
                    var ProfileTask = User.UpdateUserProfileAsync(profile);
                    yield return new WaitUntil(predicate: () => ProfileTask.IsCompleted);

                    if (ProfileTask.Exception != null)
                    {
                        Debug.LogWarning(message: $"Failed to register task with {ProfileTask.Exception}");
                        FirebaseException firebaseEx = ProfileTask.Exception.GetBaseException() as FirebaseException;
                        AuthError errorCode = (AuthError)firebaseEx.ErrorCode;
                        warningRegisterText.text = "Username Set Failed!";
                    }
                    else
                    {
                        UiManager.instance.ShowWindow("login");
                        warningRegisterText.text = "";
                    }
                }
            }
        }
    }

    /// <summary>
    /// Implementation of standard Firebase solution for "Forgot Password" situation
    /// </summary>
    /// <param name="_email"></param>
    /// <returns></returns>
    private IEnumerator Forgot(string _email)
    {
        if (_email == "")
        {
            warningForgotText.text = "Enter your e-mail, please";
        }
        else
        {
            var ForgotTask = auth.SendPasswordResetEmailAsync(_email);
            yield return new WaitUntil(predicate: () => ForgotTask.IsCompleted);
            if (ForgotTask.Exception != null)
            {
                Debug.LogWarning(message: $"Failed to forgot task with {ForgotTask.Exception}");
                FirebaseException firebaseEx = ForgotTask.Exception.GetBaseException() as FirebaseException;
                AuthError errorCode = (AuthError)firebaseEx.ErrorCode;
                string message = "Forgot Failed!";
                switch (errorCode)
                {
                    case AuthError.MissingEmail:
                        message = "Missing Email";
                        break;
                    case AuthError.MissingPassword:
                        message = "Missing Password";
                        break;
                    case AuthError.WrongPassword:
                        message = "Wrong Password";
                        break;
                    case AuthError.InvalidEmail:
                        message = "Invalid Email";
                        break;
                    case AuthError.UserNotFound:
                        message = "Account does not exist";
                        break;
                }
                warningForgotText.text = message;
            }
            else
            {
                Debug.LogFormat("Email sent check your email: {0}", _email);
                warningForgotText.text = "";
                confirmForgotText.text = "Email sent";
            }
        }
    }

    /// <summary>
    /// Writing to Firebase database and translating  'User' into json
    /// </summary>
    /// <returns></returns>
    private IEnumerator RecordDatabase()
    {
        string json = JsonUtility.ToJson(userData);
        //var DBTask = DBreference.Child("users").Child(User.UserId).SetValueAsync(json);
        var DBTask = DBreference.Child("users").Child(User.UserId).SetRawJsonValueAsync(json);
        yield return new WaitUntil(predicate: () => DBTask.IsCompleted);
        if (DBTask.Exception != null)
        {
            Debug.LogWarning(message: $"Failed to register task with {DBTask.Exception}");
        }
    }

    /// <summary>
    /// Reading json from Firebase database. Deserialization of json to the 'User' class.
    /// </summary>
    /// <returns></returns>
    private IEnumerator ReadDatabase()
    {
        var DBTask = DBreference.Child("users").Child(User.UserId).GetValueAsync();
        yield return new WaitUntil(predicate: () => DBTask.IsCompleted);
        if (DBTask.Exception != null)
        {
            Debug.LogWarning(message: $"Failed to register task with {DBTask.Exception}");
        }
        else
        {
            DataSnapshot snapshot = DBTask.Result;
            userData = JsonUtility.FromJson<User>(snapshot.GetRawJsonValue()); 
            if (userData == null)
            {
                userData = new User(User.DisplayName, new List<Note>());
            }
        }
    }

    /// <summary>
    /// Upload an image to a Firebase server
    /// </summary>
    /// <param name="_image"></param>
    /// <param name="_uid"></param>
    /// <returns></returns>
    private IEnumerator UploadImage(Texture2D _image, int _uid)
    {
        var storage = FirebaseStorage.DefaultInstance;
        var imageReference = storage.GetReference($"/images/{User.DisplayName}/{_uid}/{System.Guid.NewGuid()}.png");
        var bytes = _image.EncodeToPNG();
        var uploadTask = imageReference.PutBytesAsync(bytes);
        yield return new WaitUntil(() => uploadTask.IsCompleted);

        if (uploadTask.Exception != null)
        {
            Debug.LogError($"Failed to upload because {uploadTask.Exception}");
            yield break;
        }

        var getUrlTask = imageReference.GetDownloadUrlAsync();
        yield return new WaitUntil(() => getUrlTask.IsCompleted);

        if (getUrlTask.Exception != null)
        {
            Debug.LogError($"Failed to upload because {getUrlTask.Exception}");
            yield break;
        }

    }

}