using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.IO;

public class NoteManager : MonoBehaviour
{
    public TMP_InputField nameField;
    public TMP_InputField descriptionField;
    public static NoteManager instance;
    private int uid;
    private Note note;
    public GameObject deleteBtn;
    public GameObject loadImageBtn;
    
    void Awake()
    {
        instance = this;
    }
    /// <summary>
    /// Populate note data from an array to text fields
    /// </summary>
    /// <param name="_uid"></param>
    public void ShowNote(int _uid=-1)
    {
        uid = _uid;
        if (uid > -1)
        {
            foreach(Note _note in FirebaseManager.userData.notes)
            {
                if (_note.uid == uid)
                {
                    note = _note;
                }
            }
            
            nameField.text = note.name;
            descriptionField.text = note.description;
            deleteBtn.SetActive(true);
        }
        else
        {
            nameField.text = "";
            descriptionField.text = "";
            deleteBtn.SetActive(false);
        }
    }

    /// <summary>
    /// Click on the save button. Depending on whether this is a new note or has been edited, a pattern element is created or replaced.
    /// </summary>
    public void ClickSave()
    {
        if (uid > -1)
        {
            note.name = nameField.text;
            note.description = descriptionField.text;
        }
        else
        {
            long milliseconds = System.DateTime.Now.Ticks / System.TimeSpan.TicksPerMillisecond;
            Note note = new Note(FirebaseManager.userData.notes.Count, nameField.text, descriptionField.text, milliseconds);
            FirebaseManager.userData.notes.Add(note);
        }
        //Renew all notes
        UiManager.instance.ShowWindow("main");
    }

    /// <summary>
    /// Deletes an array 'notes' element when clicking the delete button
    /// </summary>
    public void ClickDelete()
    {
        if (uid > -1)
        {
            for (int i = 0; i < FirebaseManager.userData.notes.Count; i++)
            {
                if (note.uid == FirebaseManager.userData.notes[i].uid)
                {
                    FirebaseManager.userData.notes.RemoveAt(i);
                    break;
                }
            }
        }
        UiManager.instance.ShowWindow("main");
    }

    /// <summary>
    /// When clicking, load the image. We call the picture selection function.
    /// </summary>
    public void ClickLoad()
    {
        PickImage(2048);
    }

    /// <summary>
    /// Select pictures from local storage (PC + Android)
    /// </summary>
    /// <param name="maxSize"></param>
    private void PickImage(int maxSize)
    {
        NativeGallery.Permission permission = NativeGallery.GetImageFromGallery((path) =>
        {
            Debug.Log("Image path: " + path);
            if (path != null)
            {
                // Create Texture from selected image
                Texture2D texture = new Texture2D(2, 2);
                texture.LoadImage(File.ReadAllBytes(path));

                if (texture == null)
                {
                    Debug.Log("Couldn't load texture from " + path);
                    return;
                }
                //Load selected texture into firebase storage
                FirebaseManager.instance.StartUploadImage(texture, uid);
            }
        }, "Select a image", "image/");

        Debug.Log("Permission result: " + permission);
    }

}
