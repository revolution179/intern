using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public class Note
{
    public int uid;
    public string name;
    public string description;
    public long date; //date in milliseconds

    public Note(int _uid, string _name, string _description, long _date)
    {
        uid = _uid;
        name = _name;
        description = _description;
        date = _date;
    }
}
